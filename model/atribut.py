class Atribut:
    def __init__(self, naziv, vrednost, informacioni_resurs, atributi=[], atribut=None):
        self.naziv = naziv
        self.vrednost = vrednost
        self.informacioni_resurs = informacioni_resurs
        self.atributi = atributi
        # popunjeno kod kompozitnih atributa
        self.atribut = atribut

    @property
    def parent(self):
        return self.atribut if self.atribut is not None else self.informacioni_resurs

    @property
    def children(self):
        return self.atributi

    def dodaj_atribut(self, atribut):
        self.atributi.append(atribut)
        atribut.atribut = self

    