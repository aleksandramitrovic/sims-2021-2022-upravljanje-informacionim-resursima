from model.resurs import Resurs


class Kolekcija(Resurs):
    def __init__(self, naziv, autor, radni_prostor, kolekcija=None, resursi=[]):
        super().__init__(naziv, autor, radni_prostor, kolekcija)
        # u resurse spadaju i druge kolekcije i informacioni resursi
        self.resursi = resursi

    @property
    def parent(self):
        return self.kolekcija if self.kolekcija else self.radni_prostor

    @property
    def children(self):
        return self.resursi

    # TODO: izdvojiti liste kolekcija i informacionih resursa unutar kolekcije
    # resiti upotrebom property-ja


    def dodaj_resurs(self, resurs):
        self.resursi.append(resurs)
        resurs.kolekcija = self

    