class Autor:
    def __init__(self, ime, prezime, email, radni_prostori=[], resursi=[]):
        self.ime = ime
        self.prezime = prezime
        self.email = email
        self.radni_prostori = radni_prostori
        self.resursi = resursi

    def dodaj_readni_prostor(self, radni_prostor):
        self.radni_prostori.append(radni_prostor)
        radni_prostor.autor = self

    def dodaj_resurs(self, resurs):
        self.resursi.append(resurs)
        resurs.autor = self

    def dodaj_resurse(self, resursi):
        for resurs in resursi:
            self.dodaj_resurs(resurs)
