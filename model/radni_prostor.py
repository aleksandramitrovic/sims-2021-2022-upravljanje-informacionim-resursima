class RadniProstor:
    def __init__(self, autor, resursi=[]):
        self.autor = autor
        self.resursi = resursi

    @property
    def parent(self):
        return None

    @property
    def children(self):
        return self.resursi

    def dodaj_resurs(self, resurs):
        self.resursi.append(resurs)
        resurs.radni_prostor = self

    def dodaj_resurse(self, resursi):
        self.resursi.extend(resursi)