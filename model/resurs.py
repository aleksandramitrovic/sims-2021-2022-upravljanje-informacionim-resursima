# apstraktna klasa
# TODO: nasledjivanje ABC klase
from abc import ABC


class Resurs(ABC):
    def __init__(self, naziv, autor, radni_prostor=None, kolekcija=None):
        self.naziv = naziv
        self.autor = autor
        self.radni_prostor = radni_prostor
        self.kolekcija = kolekcija

    @property
    def parent(self):
        raise NotImplementedError()

    @property
    def children(self):
        raise NotImplementedError()