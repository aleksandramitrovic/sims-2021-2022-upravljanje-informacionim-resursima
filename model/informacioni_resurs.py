from .resurs import Resurs

class InformacioniResurs(Resurs):
    def __init__(self, naziv, autor, kolekcija, atributi=[]):
        super().__init__(naziv, autor, None, kolekcija=kolekcija)
        self.atributi = atributi

    def ispisi_podatke(self):
        raise NotImplementedError()

    @property
    def parent(self):
        return self.kolekcija

    # FIXME: Ovo je primer settera za property
    # @parent.setter
    # def parent(self, value):
    #     self.radni_prostor = value

    @property
    def children(self):
        return self.atributi

    def dodaj_atribut(self, atribut):
        self.atributi.append(atribut)
        atribut.informacioni_resurs = self