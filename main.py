from re import I
import sys
from PySide2.QtWidgets import QApplication
from gui.main_window import MainWindow
from authentication.ui.login_dialog import LoginDialog
from authentication.controller.login_controller import LoginController
# Napravimo QAplication
application = QApplication(sys.argv)
# pokrenuti (prikazati) glavni prozor
login_controller = LoginController()
login = LoginDialog(controller=login_controller)
result = login.exec()
if result == 1:
    # FIXME: proveriti zasto se aplikacija zatvara
    # login_controller.start_app()
    main_window = MainWindow(user=login.login_model)
    main_window.show()
else:
    sys.exit()
# pokrenuti aplikaciju
sys.exit(application.exec_())