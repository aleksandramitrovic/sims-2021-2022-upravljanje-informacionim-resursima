from msilib.schema import File
from PySide2.QtWidgets import QMainWindow, QLabel
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt

# lokalno definisane klase
from gui.widgets.menu_bar import MenuBar
from gui.widgets.structure_dock_widget import StructureDockWidget
from gui.widgets.tool_bar import ToolBar
from gui.widgets.status_bar import StatusBar
from gui.widgets.central_widget import CentralWidget
# from gui.widgets.file_dock_widget import FileDockWidget

class MainWindow(QMainWindow):
    def __init__(self, parent=None, user=None):
        # poziv super inicijalizaotra (QMainWindow)
        super().__init__(parent)
        # Osnovna podesavanja glavnog prozora
        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("resources/icons/blue-document.png"))
        self.resize(1000, 650)

        # inicijalizacija osnovnih elemenata GUI-ja
        self.menu_bar = MenuBar(self)
        self.tool_bar = ToolBar(self)
        self.status_bar = StatusBar(self)
        self.central_widget = CentralWidget(self)
        self.file_dock_widget = StructureDockWidget("Struktura radnog prostora", self)

        # uvezivanje elemenata GUI-ja
        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.file_dock_widget, Qt.Vertical)

        # sacuvavanje prijavljenog korisnika iz dijaloga
        self.user = user
        self.status_bar.addWidget(QLabel("Ulogovani korisnik: " + self.user.user_id.upper()))