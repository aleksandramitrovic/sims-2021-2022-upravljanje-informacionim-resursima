from PySide2.QtWidgets import QDockWidget
from .resource_structure.view.resource_structure_widget import ResourceStrucutreWidget

class StructureDockWidget(QDockWidget):
    def __init__(self, title="", parent=None):
        super().__init__(title, parent)
        self.resource_structure_widget = ResourceStrucutreWidget(self)

        self.setWidget(self.resource_structure_widget)

    