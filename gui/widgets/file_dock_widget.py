from PySide2.QtWidgets import QDockWidget, QFileSystemModel, QTreeView
from PySide2.QtCore import QDir

class FileDockWidget(QDockWidget):
    # TODO: objasniti dockwidget sledeci put i Model-View widgete
    def __init__(self, title="", parent=None) -> None:
        super().__init__(title, parent)
        self.file_model = QFileSystemModel()
        self.file_model.setRootPath(QDir.currentPath())
        self.tree_view = QTreeView()
        self.tree_view.setModel(self.file_model)

        self.setWidget(self.tree_view)

    