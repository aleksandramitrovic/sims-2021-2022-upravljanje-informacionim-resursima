from PySide2.QtWidgets import QToolBar


class ToolBar(QToolBar):
    def __init__(self, title="", parent=None):
        super().__init__(title, parent)