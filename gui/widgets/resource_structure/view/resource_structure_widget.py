from gui.widgets.resource_structure.model.resource_structure_model import ResourceStructureModel

from PySide2.QtWidgets import QWidget, QTreeView
from model.atribut import Atribut
from model.autor import Autor
from model.informacioni_resurs import InformacioniResurs
from model.kolekcija import Kolekcija
from model.radni_prostor import RadniProstor


class ResourceStrucutreWidget(QWidget):
    def __init__(self, parent=None, model=None):
        # TODO: dodati treeview
        # TODO: Inicijalizovati model (za sada hardkodovan)
        # TODO: setovati view-u model
        # TODO: smestitit instancu ovog widget-a u levi dockwidget na glavnom prozoru
        super().__init__(parent)
        self.tree_view = QTreeView(self)
        self.structure_model = self._dummy_model() #model
        self.tree_view.setModel(self.structure_model)
        


    # FIXME: POTREBNO JE IZBRISATI kada bude bio kreiran model spram podataka u datoteci/bazi podataka
    def _dummy_model(self):
        author = Autor("Aleksandra", "Mitrovic", "amitrovic@singidunum.ac.rs")
        workspace = RadniProstor(author)
        coll_1 = Kolekcija("Kolekcija 1", author, workspace)
        coll_2 = Kolekcija("Kolekcija 2", author, workspace)
        coll_3 = Kolekcija("Kolekcija 2.1", author, None, coll_2)
        inf_res_1 = InformacioniResurs("Inf. resurs 1", author, coll_1)
        inf_res_2 = InformacioniResurs("Inf. resurs 2", author, coll_1)
        inf_res_3 = InformacioniResurs("Inf. resurs 3", author, coll_3)
        attr_1 = Atribut("Atribut 1", 10, inf_res_1)
        # FIXME: prepraviti da atribut ima ili inf_res ili atribut kao roditelja (nije dobra implementacija za sada)
        # attr_2 = Atribut("Atribut 1.1", 20, )

        inf_res_1.dodaj_atribut(attr_1)
        coll_1.dodaj_resurs(inf_res_1)
        coll_1.dodaj_resurs(inf_res_2)
        coll_3.dodaj_resurs(inf_res_3)
        coll_2.dodaj_resurs(coll_3)
        
        author.dodaj_readni_prostor(workspace)
        author.dodaj_resurse([coll_1, coll_2, coll_3, inf_res_1, inf_res_2, inf_res_3])
        workspace.dodaj_resurse([coll_1, coll_2])

        return ResourceStructureModel(workspace)