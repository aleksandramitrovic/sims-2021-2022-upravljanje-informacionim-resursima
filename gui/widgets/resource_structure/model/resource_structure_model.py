from PySide2.QtCore import QAbstractItemModel, QModelIndex, Qt
from PySide2.QtGui import QIcon
from model.atribut import Atribut
from model.informacioni_resurs import InformacioniResurs

from model.kolekcija import Kolekcija
# Napraviti model po uzoru na QAbstractItemModel (za pocetak read-only)
class ResourceStructureModel(QAbstractItemModel):
    # workspace je radni prostor iz modela
    def __init__(self, workspace=None, parent=None):
        super().__init__(parent)
        self.workspace = workspace # korenski element stabla

    def get_element(self, index):
        if (index.isValid()):
            element = index.internalPointer() # dobavljanje vrednosti na indeksu
            if element:
                return element
        return self.workspace

    # TODO: Za read-only model implementirati sledece metode:
    def index(self, row, column, parent=QModelIndex()):
        element = self.get_element(parent)
        print(element, row, column)
        if (row >= 0 and row < len(element.children)) and element.children[row]:
            return self.createIndex(row, column, element.children[row])
        return QModelIndex() # nevalidan indeks

    def parent(self, child_index):
        # vraca indeks roditelja od prosledjenog child_indeksa
        # pronaci sta se nalazi na child_index-u
        if (child_index.isValid()):
            child = self.get_element(child_index)
            parent = child.parent # automatski pozvalo property metodu koja se zove parent
            if parent and (parent != self.workspace) and parent.parent:
                return self.createIndex(parent.parent.children.index(parent), 0, parent)
                                                #parent.children (parent - property, children - property)
            # TODO: nije zavrseno
        return QModelIndex() # nevalidan indeks

    def rowCount(self, parent=QModelIndex()):
        element = self.get_element(parent)
        if element:
            return len(element.children)
        return len(self.workspace.children) # u slucaju kada ne zelimo da prikazemo korenski element

    def columnCount(self, parent=QModelIndex()):
        return 1

    def data(self, index, role=Qt.DisplayRole):
        element = self.get_element(index)
        if type(element) == Kolekcija:
            if index.column() == 0 and role == Qt.DisplayRole:
                return element.naziv
            if index.column() == 0 and role == Qt.DecorationRole:
                return QIcon("resources/icons/folder.png")
        elif type(element) == InformacioniResurs:
            if index.column() == 0 and role == Qt.DisplayRole:
                return element.naziv # ponavljanje
            if index.column() == 0 and role == Qt.DecorationRole:
                return QIcon("resources/icons/document.png")
        elif type(element) == Atribut:
            if index.column() == 0 and role == Qt.DisplayRole:
                return element.naziv # opet ponavljanje
            if index.column() == 0 and role == Qt.DecorationRole:
                return QIcon("resources/icons/asterisk.png")

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if section == 0 and orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return "Naziv"
        return None
        # return super().headerData(section, orientation, role)
        

    # TODO: Editable model
    # def flags(self, index):
    #     # FIXME: izmeniti
    #     return super().flags(index)

    # def setData(self, index, value, role: Qt.DisplayRole):
    #     # FIXME: izmeniti
    #     return super().setData(index, value, role)