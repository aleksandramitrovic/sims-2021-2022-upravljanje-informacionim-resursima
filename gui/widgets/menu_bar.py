from PySide2.QtWidgets import QMenuBar, QMenu

class MenuBar(QMenuBar):
    def __init__(self, parent=None):
        # poziv super inicijalizatora
        super().__init__(parent)

        # kreiranje osnovnih menija
        self.file_menu = QMenu("File", self)
        self.edit_menu = QMenu("Edit", self)
        self.help_menu = QMenu("Help", self)

        # povezivanje menija
        self._poulate_menues()

    def _poulate_menues(self):
        self.addMenu(self.file_menu)
        self.addMenu(self.edit_menu)
        self.addMenu(self.help_menu)